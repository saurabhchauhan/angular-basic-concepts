import { AngularBasicConceptsPage } from './app.po';

describe('angular-basic-concepts App', () => {
  let page: AngularBasicConceptsPage;

  beforeEach(() => {
    page = new AngularBasicConceptsPage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
