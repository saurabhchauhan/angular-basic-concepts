import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuindingMethod3Component } from './buinding-method3.component';

describe('BuindingMethod3Component', () => {
  let component: BuindingMethod3Component;
  let fixture: ComponentFixture<BuindingMethod3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuindingMethod3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuindingMethod3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
