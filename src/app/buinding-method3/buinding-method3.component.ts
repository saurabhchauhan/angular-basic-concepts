import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-buinding-method3',
  templateUrl: './buinding-method3.component.html',
  styleUrls: ['./buinding-method3.component.css']
})
export class BuindingMethod3Component implements OnInit {
  form2: FormGroup;

  constructor(fb1: FormBuilder) {
    this.form2 = fb1.group({
      "firstName": this.firstName,
      "password": ["", Validators.required]
    });

  }

  ngOnInit() {
  }

  firstName = new FormControl("", Validators.required);

  onSubmitModelBased() {
    console.log("model-based form submitted");
    console.log(this.form2);
  }
}
