import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-buinding-method2',
  templateUrl: './buinding-method2.component.html',
  styleUrls: ['./buinding-method2.component.css']
})
export class BuindingMethod2Component implements OnInit {

  form1: FormGroup;
  constructor( @Inject(FormBuilder) fb: FormBuilder) {
    this.form1 = fb.group({
      name1: fb.group({
        first: ['Nancy', Validators.minLength(2)],
        last: 'Drew',
      }),
      email: ['', Validators.email],
    });
    console.log(this.form1);
  }

  ngOnInit() {
  }
  submitFormBuilder() {
    console.log(this.form1)
  }
}
