import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuindingMethod2Component } from './buinding-method2.component';

describe('BuindingMethod2Component', () => {
  let component: BuindingMethod2Component;
  let fixture: ComponentFixture<BuindingMethod2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuindingMethod2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuindingMethod2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
