import { Component, OnInit } from '@angular/core';
import { User } from '../model/user'
@Component({
  selector: 'app-buinding-method1',
  templateUrl: './buinding-method1.component.html',
  styleUrls: ['./buinding-method1.component.css']
})
export class BuindingMethod1Component implements OnInit {
  public user: User = new User();

  constructor() {
    this.user.firstName = 'sam';
    this.user.password = 'nik';
  }

  ngOnInit() {
  }

  onSubmit() {
    console.log(this.user);
  }
}

