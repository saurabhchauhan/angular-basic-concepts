import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuindingMethod1Component } from './buinding-method1.component';

describe('BuindingMethod1Component', () => {
  let component: BuindingMethod1Component;
  let fixture: ComponentFixture<BuindingMethod1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuindingMethod1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuindingMethod1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
