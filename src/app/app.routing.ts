import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AppWebRoutes } from './app-web-routes';
import { SidebarComponent } from './sidebar/sidebar.component';
import { BuindingMethod1Component } from './buinding-method1/buinding-method1.component';
import { BuindingMethod2Component } from './buinding-method2/buinding-method2.component';
import { BuindingMethod3Component } from './buinding-method3/buinding-method3.component';

const dashboardRoutes = [
    { path: AppWebRoutes.SIDEBAR, component: SidebarComponent },
    { path: AppWebRoutes.BUINDING_METHOD_ONE, component: BuindingMethod1Component },
    { path: AppWebRoutes.BUINDING_METHOD_TWO, component: BuindingMethod2Component },
    { path: AppWebRoutes.BUINDING_METHOD_THREE, component: BuindingMethod3Component }
]

const appRoutes: Routes = [
    { path: '', component: DashboardComponent, children: dashboardRoutes },
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);