import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
//routing
import { routing } from './app.routing';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { TopNavbarComponent } from './top-navbar/top-navbar.component';
import { BuindingMethod1Component } from './buinding-method1/buinding-method1.component';
import { BuindingMethod2Component } from './buinding-method2/buinding-method2.component';
import { BuindingMethod3Component } from './buinding-method3/buinding-method3.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    SidebarComponent,
    TopNavbarComponent,
    BuindingMethod1Component,
    BuindingMethod2Component,
    BuindingMethod3Component
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    routing
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
