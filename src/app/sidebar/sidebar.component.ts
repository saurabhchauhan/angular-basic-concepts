import { Component, OnInit } from '@angular/core';
import { AppWebRoutes } from '../app-web-routes';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  constructor(private _router: Router) { }

  ngOnInit() {
  }

  public webRoutes = [
    { path: AppWebRoutes.BUINDING_METHOD_ONE, itemName: "Data Buinding One" },
    { path: AppWebRoutes.BUINDING_METHOD_TWO, itemName: "Data Buinding Two" },
    { path: AppWebRoutes.BUINDING_METHOD_THREE, itemName: "Data Buinding Three" },
  ];

  navigate(routes: any) {
    this._router.navigateByUrl(routes.path);
  }
}
